import java.util.Scanner;

public class DaysPerMonth {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the year you want to check: "); // вводимо рік
        int year = in.nextInt();

        System.out.print("Enter the serial number of the month you want to check: "); // вводимо місяць
        int month = in.nextInt();

        int numDays = 0;
        String monthName = "Unknown";

        switch (month) { // пробігаємось по кожному місяцю, присвоюємо йому назву та кількість днів
            case 1:
                monthName = "January";
                numDays = 31;
                break;
            case 2: // перевіряємо лютий на високосний рік
                monthName = "February";
                if ((year % 400 == 0) || (year % 4 == 0) && (year % 100 != 0)) {
                    numDays = 29;
                } else {
                    numDays = 28;
                }
                break;
            case 3:
                monthName = "March";
                numDays = 31;
                break;
            case 4:
                monthName = "April";
                numDays = 30;
                break;
            case 5:
                monthName = "May";
                numDays = 31;
                break;
            case 6:
                monthName = "June";
                numDays = 30;
                break;
            case 7:
                monthName = "July";
                numDays = 31;
                break;
            case 8:
                monthName = "August";
                numDays = 31;
                break;
            case 9:
                monthName = "September";
                numDays = 30;
                break;
            case 10:
                monthName = "October";
                numDays = 31;
                break;
            case 11:
                monthName = "November";
                numDays = 30;
                break;
            case 12:
                monthName = "November";
                numDays = 31;
                break;
            default: // передбачаємо сигнал, якщо неправильно введено номер місяця (більше 12)
                System.out.println("Non-existed month");
        }
        System.out.println("In " + monthName + ", " + year + " is " + numDays + " days");
    }
}